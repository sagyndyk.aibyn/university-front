import React, {useState, useEffect}from 'react';
import axios from "axios";
import {Link, useParams} from "react-router-dom";

export default function GetSubject(){

    const [subjects, setSubjects]=useState([])

    const {id}=useParams()

    useEffect(()=>{
        loadSubject();
    },[])

    const loadSubject=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/subject/")
        setSubjects(result.data);
    }

    const deleteSubject=async (id)=>{
        await axios.delete(`http://localhost:8080/api/test/subject/delete/${id}`)
        loadSubject();
    }

    return(
        <div className='container'>
            <div className="py-4">
                <table className="table table-striped rounded shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Speciality</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        subjects.map((subject,index)=>(
                            <tr>
                                <th>{subject.id}</th>
                                <td>{subject.name}</td>
                                <td>{subject.speciality.name}</td>
                                <td>
                                    <Link className="btn btn-outline-primary  mx-2" to={`/edit-subject/${subject.id}`}>Edit</Link>
                                    <button className="btn btn-danger mx-2" onClick={()=>deleteSubject(subject.id)}>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link className="btn btn-outline-success" to="/add-subject">Add Subject</Link>
            </div>
        </div>
    )
}