import React, {useState, useEffect} from "react";
import axios from "axios";
import{ Link, useNavigate } from "react-router-dom"

export default function AddSubject(){

    let navigate=useNavigate();

    const [specialities, setSpecialities]=useState([])

    useEffect(()=>{
        loadSpeciality();
    },[])

    const loadSpeciality=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/speciality/")
        setSpecialities(result.data);
    }

    const [subject, setSubject]=useState({
        name: "",
        speciality: null
    })


    const{name, speciality}=subject;

    const onInputChange=(e)=>{
        setSubject({...subject,[e.target.name]:e.target.value})
    };

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/api/test/subject/create",subject)
        navigate("/get-subjects")

    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Add Subject</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Subject Name"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Speciality" className="form-label">
                            Speciality
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Subject Speciality"
                            name="speciality"
                            value={speciality}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-subjects">Cancel</Link>
                </form>
            </div>
        </div>

        <div className="py-4">
            <table className="table table-striped rounded shadow">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Speciality</th>
                </tr>
                </thead>
                <tbody>

                {
                    specialities.map((speciality,index)=>(
                        <tr>
                            <th>{speciality.id}</th>
                            <td>{speciality.name}</td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
        </div>
    </div>
}