import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

import Navbar from "./layout/Navbar";
import Home from "./pages/Home";
import GetSpeciality from "./specialities/GetSpeciality";
import AddSpeciality from "./specialities/AddSpeciality";
import EditSpeciality from "./specialities/EditSpeciality";
import GetGroup from "./groups/GetGroup";
import AddGroup from "./groups/AddGroup";
import EditGroup from "./groups/EditGroup";
import GetStudent from "./students/GetStudent";
import AddStudent from "./students/AddStudent";
import EditStudent from "./students/EditStudent";
import GetSubject from "./subjects/GetSubject";
import AddSubject from "./subjects/AddSubject"
import EditSubject from "./subjects/EditSubject";
import GetTeacher from "./teachers/GetTeacher";
import AddTeacher from "./teachers/AddTeacher";
import EditTeacher from "./teachers/EditTeacher"

function App() {
  return (
    <div className="App">
        <Router>
            <Navbar/>

            <Routes>
                <Route exact path="/" element={<Home/>}/>
                <Route exact path="/get-specialities" element={<GetSpeciality/>}/>
                <Route exact path="/add-speciality" element={<AddSpeciality/>}/>
                <Route exact path="/edit-speciality/:id" element={<EditSpeciality/>}/>
                <Route exact path="/get-groups" element={<GetGroup/>}/>
                <Route exact path="/add-group" element={<AddGroup/>}/>
                <Route exact path="/add-student" element={<AddStudent/>}/>
                <Route exact path="/edit-group/:id" element={<EditGroup/>}/>
                <Route exact path="/get-students" element={<GetStudent/>}/>
                <Route exact path="/edit-student/:id" element={<EditStudent/>}/>
                <Route exact path="/get-subjects" element={<GetSubject/>}/>
                <Route exact path="/add-subject" element={<AddSubject/>}/>
                <Route exact path="/edit-subject/:id" element={<EditSubject/>}/>
                <Route exact path="/get-teachers" element={<GetTeacher/>}/>
                <Route exact path="/add-teacher" element={<AddTeacher/>}/>
                <Route exact path="/edit-teacher/:id" element={<EditTeacher/>}/>

            </Routes>
        </Router>
    </div>
  );
}

export default App;
