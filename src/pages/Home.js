import React, {useState, useEffect}from 'react';

export default function Home(){

    return(
        <div className='container'>
            <div className="form-control mt-4 shadow rounded">
                <h1 className="text-center">University Management System</h1>
            </div>

                <div className="card mt-4 shadow rounded">
                        <div className="card-body">
                            <h5 className="card-title">Speciality List</h5>
                            <p className="card-text">You can add, edit or delete specialities.</p>
                            <a href="http://localhost:3000/get-specialities" className="btn btn-primary">Specialities</a>
                        </div>
                </div>

            <div className="row">
                <div className="col-sm-6">
            <div className="card mt-4 shadow rounded">
                <div className="card-body">
                    <h5 className="card-title">Group List</h5>
                    <p className="card-text">You can add, edit or delete groups.</p>
                    <a href="http://localhost:3000/get-groups" className="btn btn-primary">Groups</a>
                </div>
            </div>
                </div>

                <div className="col-sm-6">
            <div className="card mt-4 shadow rounded">
                <div className="card-body">
                    <h5 className="card-title">Student List</h5>
                    <p className="card-text">You can add, edit or delete students.</p>
                    <a href="http://localhost:3000/get-students" className="btn btn-primary">Students</a>
                </div>
            </div>
                </div>


                <div className="row">
                    <div className="col-sm-6">
            <div className="card mt-4 shadow rounded">
                <div className="card-body">
                    <h5 className="card-title">Subject List</h5>
                    <p className="card-text">You can add, edit or delete subjects.</p>
                    <a href="http://localhost:3000/get-subjects" className="btn btn-primary">subjects</a>
                </div>
            </div>
                    </div>

                    <div className="col-sm-6">
            <div className="card mt-4 shadow rounded">
                <div className="card-body">
                    <h5 className="card-title">Teacher List</h5>
                    <p className="card-text">You can add, edit or delete teachers.</p>
                    <a href="http://localhost:3000/get-teachers" className="btn btn-primary">Teachers</a>
                </div>
            </div>
            </div>
                </div>
            </div>
        </div>
    )
}