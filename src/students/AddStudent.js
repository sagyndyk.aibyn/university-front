import React, {useState, useEffect} from "react";
import axios from "axios";
import {Link, useNavigate} from "react-router-dom";

export default function EditStudent(){

    const [groups, setGroups]=useState([])

    useEffect(()=>{
        loadGroup();
    },[])

    const loadGroup=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/group/")
        setGroups(result.data);
    }

    let navigate=useNavigate();

    const [student, setStudent]=useState({
        name: "",
        surname: "",
        group: null
    })

    const{name, surname, group}=student;

    const onInputChange=(e)=>{
        setStudent({...student,[e.target.name]:e.target.value})
    };

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/api/test/student/create",student)
        navigate("/get-students")

    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Add Student</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Student Name"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Surname" className="form-label">
                            Surname
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Student Surname"
                            name="surname"
                            value={surname} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="group" className="form-label">
                            Group
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Student Group "
                            name="group"
                            value={group}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-students">Cancel</Link>
                </form>
            </div>
        </div>

        <div className="py-4">
            <table className="table table-striped rounded shadow">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Group</th>
                    <th scope="col">Speciality</th>
                </tr>
                </thead>
                <tbody>

                {
                    groups.map((group)=>(
                        <tr>
                            <th>{group.id}</th>
                            <td>{group.name}</td>
                            <td>{group.speciality.name}</td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
        </div>

    </div>
}