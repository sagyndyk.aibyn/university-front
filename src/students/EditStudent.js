import React, {useEffect, useState} from "react";
import axios from "axios";
import{ Link, useNavigate, useParams} from "react-router-dom"

export default function EditStudent(){

    let navigate=useNavigate();

    const {id}=useParams()

    const [student, setStudent]=useState({
        name: "",
        surname: "",
        group: null
    })

    const{name, surname, group}=student;

    const onInputChange=(e)=>{
        setStudent({...student,[e.target.name]:e.target.value})
    };

    useEffect(()=>{
        loadUser()
    },[])

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.put(`http://localhost:8080/api/test/student/update/${id}`,student)
        navigate("/get-students")
    }

    const loadUser=async ()=>{
        const result=await axios.get(`http://localhost:8080/api/test/student/update/${id}`)
        setStudent(result.data)
    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Edit Student</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Student Surname"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Surname" className="form-label">
                            Surname
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Student Surname"
                            name="surname"
                            value={surname} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Group" className="form-label">
                            Group
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Student Group"
                            name="group"
                            value={group}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-students">Cancel</Link>
                </form>
            </div>
        </div>
    </div>
}