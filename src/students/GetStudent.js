import React, {useState, useEffect}from 'react';
import axios from "axios";
import {Link, useParams} from "react-router-dom";

export default function GetStudent(){

    const [students, setStudents]=useState([])

    const {id}=useParams()

    useEffect(()=>{
        loadStudent();
    },[])

    const loadStudent=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/student/")
        setStudents(result.data);
    }

    const deleteStudent=async (id)=>{
        await axios.delete(`http://localhost:8080/api/test/student/delete/${id}`)
        loadStudent();
    }

    return(
        <div className='container'>
            <div className="py-4">
                <table className="table table-striped rounded shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Group</th>
                        <th scope="col">Speciality</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        students.map((student,index)=>(
                            <tr>
                                <th>{student.id}</th>
                                <td>{student.name}</td>
                                <td>{student.surname}</td>
                                <td>{student.group.name}</td>
                                <td>{student.group.speciality.name}</td>
                                <td>
                                    <Link className="btn btn-outline-primary  mx-2" to={`/edit-student/${student.id}`}>Edit</Link>
                                    <button className="btn btn-danger mx-2" onClick={()=>deleteStudent(student.id)}>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link className="btn btn-outline-success" to="/add-student">Add Student</Link>
            </div>
        </div>
    )
}