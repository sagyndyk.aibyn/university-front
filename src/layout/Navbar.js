import React from 'react';
import {Link} from "react-router-dom";

export default function Navbar(){
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">University</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon">a</span>
                    </button>
                    <Link className="btn btn-outline-light" to="/get-specialities">Specialities</Link>
                    <Link className="btn btn-outline-light" to="/get-groups">Groups</Link>
                    <Link className="btn btn-outline-light" to="/get-students">Students</Link>
                    <Link className="btn btn-outline-light" to="/get-subjects">Subjects</Link>
                    <Link className="btn btn-outline-light" to="/get-teachers">Teachers</Link>
                </div>
            </nav>
        </div>
    )
}