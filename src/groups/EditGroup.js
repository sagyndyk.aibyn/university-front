import React, {useEffect, useState} from "react";
import axios from "axios";
import{ Link, useNavigate, useParams} from "react-router-dom"

export default function EditGroup(){

    let navigate=useNavigate();

    const {id}=useParams()

    const [group, setGroup]=useState({
        name: "",
        speciality: null
    })

    const{name, speciality}=group;

    const onInputChange=(e)=>{
        setGroup({...group,[e.target.name]:e.target.value})
    };

    useEffect(()=>{
        loadUser()
    },[])

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.put(`http://localhost:8080/api/test/group/update/${id}`,group)
        navigate("/get-groups")
    }

    const loadUser=async ()=>{
        const result=await axios.get(`http://localhost:8080/api/test/group/update/${id}`)
        setGroup(result.data)
    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Edit Group</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Group Name"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Speciality" className="form-label">
                            Speciality
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Group Speciality"
                            name="speciality"
                            value={speciality}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-groups">Cancel</Link>
                </form>
            </div>
        </div>
    </div>
}