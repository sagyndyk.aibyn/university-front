import React, {useState, useEffect} from "react";
import axios from "axios";
import{ Link, useNavigate } from "react-router-dom"

export default function AddGroup(){

    let navigate=useNavigate();


    const [specialities, setSpecialities]=useState([])

    useEffect(()=>{
        loadSpeciality();
    },[])


    const loadSpeciality=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/speciality/")
        setSpecialities(result.data);
    }

    const [group, setGroup]=useState({
        name: "",
        speciality: null
    })

    const{name, speciality}=group;

    const onInputChange=(e)=>{
        setGroup({...group,[e.target.name]:e.target.value})
    };

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/api/test/group/create",group)
        navigate("/get-groups")

    }

    return(
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                    <h2 className="text-center m-4">Add Group</h2>
                    <form onSubmit={(e)=>onSubmit(e)}>
                        <div className="mb-3">
                            <label htmlFor="Name" className="form-label">
                                Name
                            </label>
                            <input
                                type={"text"}
                                className="form-control"
                                placeholder="Enter Group Name"
                                name="name"
                                value={name} onChange={(e)=>onInputChange(e)}
                            />
                        </div>
                        <div className="input-group mb-3 mt-4">
                            <label htmlFor="Speciality" className="input-group-text">Speciality</label>
                            <select className="form-select">
                                <option>Select Group Speciality</option>
                                {
                                    specialities.map((spec)=>(
                                        <option value={speciality}>{spec.name}</option>
                                    ))
                                }
                            </select>

                        </div>
                        <button type="submit" className="btn btn-outline-success">Submit</button>
                        <Link className="btn btn-outline-danger mx-2" to="/get-groups">Cancel</Link>
                    </form>
                </div>
            </div>
            <div>
            </div>
        </div>
    )
}