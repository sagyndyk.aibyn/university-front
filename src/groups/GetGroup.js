import React, {useState, useEffect}from 'react';
import axios from "axios";
import {Link, useParams} from "react-router-dom";

export default function GetGroup(){

    const [groups, setGroups]=useState([])

    const {id}=useParams()

    useEffect(()=>{
        loadGroup();
    },[])

    const loadGroup=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/group/")
        setGroups(result.data);
    }

    const deleteGroup=async (id)=>{
        await axios.delete(`http://localhost:8080/api/test/group/delete/${id}`)
        loadGroup();
    }

    return(
        <div className='container'>
            <div className="py-4">
                <table className="table table-striped rounded shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Group</th>
                        <th scope="col">Speciality</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        groups.map((group,index)=>(
                            <tr>
                                <th>{group.id}</th>
                                <td>{group.name}</td>
                                <td>{group.speciality.name}</td>
                                <td>
                                    <Link className="btn btn-outline-primary  mx-2" to={`/edit-group/${group.id}`}>Edit</Link>
                                    <button className="btn btn-danger mx-2" onClick={()=>deleteGroup(group.id)}>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link className="btn btn-outline-success" to="/add-group">Add Group</Link>
            </div>
        </div>
    )
}