import React, {useEffect, useState} from "react";
import axios from "axios";
import{ Link, useNavigate, useParams} from "react-router-dom"

export default function EditTeacher(){

    let navigate=useNavigate();

    const {id}=useParams()

    const [teacher, setTeacher]=useState({
        name: "",
        surname: "",
        subject: null
    })

    const{name, surname, subject}=teacher;

    const onInputChange=(e)=>{
        setTeacher({...teacher,[e.target.name]:e.target.value})
    };

    useEffect(()=>{
        loadUser()
    },[])

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.put(`http://localhost:8080/api/test/teacher/update/${id}`,teacher)
        navigate("/get-teachers")
    }

    const loadUser=async ()=>{
        const result=await axios.get(`http://localhost:8080/api/test/teacher/update/${id}`)
        setTeacher(result.data)
    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Edit Teacher</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Teacher Surname"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Surname" className="form-label">
                            Surname
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Teacher Surname"
                            name="surname"
                            value={surname} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Subject" className="form-label">
                            Subject
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Teacher Group"
                            name="subject"
                            value={subject}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-teachers">Cancel</Link>
                </form>
            </div>
        </div>
    </div>
}