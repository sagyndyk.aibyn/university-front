import React, {useState, useEffect} from "react";
import axios from "axios";
import {Link, useNavigate} from "react-router-dom"

export default function EditTeacher(){

    const [subjects, setSubjects]=useState([])

    useEffect(()=>{
        loadSubject();
    },[])

    const loadSubject=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/subject/")
        setSubjects(result.data);
    }

    let navigate=useNavigate();

    const [teacher, setTeacher]=useState({
        name: "",
        surname: "",
        subject: null
    })

    const{name, surname, subject}=teacher;

    const onInputChange=(e)=>{
        setTeacher({...teacher,[e.target.name]:e.target.value})
    };

    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/api/test/teacher/create",teacher)
        navigate("/get-teachers")

    }

    return<div className="container">
        <div className="row">
            <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                <h2 className="text-center m-4">Add Teacher</h2>
                <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">
                            Name
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Teacher Name"
                            name="name"
                            value={name} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Surname" className="form-label">
                            Surname
                        </label>
                        <input
                            type={"text"}
                            className="form-control"
                            placeholder="Enter Teacher Surname"
                            name="surname"
                            value={surname} onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="subject" className="form-label">
                            Subject
                        </label>
                        <input
                            type={"number"}
                            className="form-control"
                            placeholder="Enter Teacher Subject "
                            name="subject"
                            value={subject}
                            onChange={(e)=>onInputChange(e)}
                        />
                    </div>
                    <button type="submit" className="btn btn-outline-success">Submit</button>
                    <Link className="btn btn-outline-danger mx-2" to="/get-teachers">Cancel</Link>
                </form>
            </div>
        </div>

        <div className="py-4">
            <table className="table table-striped rounded shadow">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Speciality</th>
                </tr>
                </thead>
                <tbody>

                {
                    subjects.map((subject,index)=>(
                        <tr>
                            <th>{subject.id}</th>
                            <td>{subject.name}</td>
                            <td>{subject.speciality.name}</td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
            <Link className="btn btn-outline-success" to="/add-subject">Add Subject</Link>
        </div>
    </div>
}