import React, {useState, useEffect}from 'react';
import axios from "axios";
import {Link, useParams} from "react-router-dom";

export default function GetTeacher(){

    const [teachers, setTeachers]=useState([])

    const {id}=useParams()

    useEffect(()=>{
        loadTeacher();
    },[])

    const loadTeacher=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/teacher/")
        setTeachers(result.data);
    }

    const deleteTeacher=async (id)=>{
        await axios.delete(`http://localhost:8080/api/test/teacher/delete/${id}`)
        loadTeacher();
    }

    return(
        <div className='container'>
            <div className="py-4">
                <table className="table table-striped rounded shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Subject</th>
                        <th scope="col">Speciality</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        teachers.map((teacher,index)=>(
                            <tr>
                                <th>{teacher.id}</th>
                                <td>{teacher.name}</td>
                                <td>{teacher.surname}</td>
                                <td>{teacher.subject.name}</td>
                                <td>{teacher.subject.speciality.name}</td>
                                <td>
                                    <Link className="btn btn-outline-primary  mx-2" to={`/edit-teacher/${teacher.id}`}>Edit</Link>
                                    <button className="btn btn-danger mx-2" onClick={()=>deleteTeacher(teacher.id)}>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link className="btn btn-outline-success" to="/add-teacher">Add Teacher</Link>
            </div>
        </div>
    )
}