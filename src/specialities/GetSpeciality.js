import React, {useState, useEffect}from 'react';
import axios from "axios";
import {Link, useParams} from "react-router-dom";

export default function GetSpeciality(){

    const [specialities, setSpecialities]=useState([])

    const {id}=useParams()

    useEffect(()=>{
        loadSpeciality();
    },[])

    const loadSpeciality=async ()=>{
        const result=await axios.get("http://localhost:8080/api/test/speciality/")
        setSpecialities(result.data);
    }

    const deleteSpeciality=async (id)=>{
        await axios.delete(`http://localhost:8080/api/test/speciality/delete/${id}`)
        loadSpeciality();
    }


    return(
        <div className='container'>
            <div className="py-4">
                <table className="table table-striped rounded shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Speciality</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        specialities.map((speciality,index)=>(
                            <tr>
                                <th>{speciality.id}</th>
                                <td>{speciality.name}</td>
                                <td>
                                    <Link className="btn btn-outline-primary  mx-2" to={`/edit-speciality/${speciality.id}`}>Edit</Link>
                                    <button className="btn btn-danger mx-2" onClick={()=>deleteSpeciality(speciality.id)}>Delete</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link className="btn btn-outline-success" to="/add-speciality">Add Speciality</Link>
            </div>
        </div>
    )
}