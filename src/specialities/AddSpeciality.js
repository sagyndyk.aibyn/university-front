import React, {useState}from 'react'
import axios from "axios";
import{ Link, useNavigate } from "react-router-dom"

export default function AddSpeciality(){

    let navigate=useNavigate()

    const [speciality, setSpeciality] = useState({
        name:""
    })

    const onInputChange=(e)=>{
        setSpeciality({...speciality,[e.target.name]:e.target.value})

    }
    
    const onSubmit=async (e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/api/test/speciality/create",speciality)
        navigate("/get-specialities");
    }
    
    const{name}=speciality

    return(
        <div className="container">
            <div className="row">
                <div className="col-md-5 offset-md-3 border rounded p-4 mt-3 shadow">
                    <h2 className="text-center m-4">Add Speciality</h2>
                    <form onSubmit={(e)=>onSubmit(e)}>
                    <div className="mb-3">
                        <label htmlFor="Name" className="form-label">Name</label>
                        <input type={"text"} className="form-control" placeholder="Enter Speciality name"
                        name="name" value={name} onChange={(e)=>onInputChange(e)}/>
                    </div>
                    <button type="submit" className="col-4 btn btn-outline-success">Submit</button>
                    <Link className="col-4 btn btn-outline-danger mx-2" to="/get-specialities" >Cancel</Link>
                    </form>
                </div>
            </div>
        </div>
    )
}